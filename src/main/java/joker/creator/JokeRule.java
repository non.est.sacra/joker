package joker.creator;

import static zoomba.lang.core.operations.Function.NIL;

/**
 */
public class JokeRule {

    Object object;

    String methodName;

    Object[] arguments;

    Object returnValue;

    Throwable except;

    private MethodHook hook;

    private MethodHook hook(){
        if ( object == null )
            throw new UnsupportedOperationException("Object instance is not set!");
        if  ( hook == null ) {
            MethodHook.HookExtractor extractor = new MethodHook.HookExtractor();
            object.equals(extractor);
            hook = extractor.hook;
        }
        return hook;
    }

    /**
     * Lets view the objects property value
     * @param propertyName name of the property
     * @return value of the property
     */
    public Object view(String propertyName){
        MethodHook hook = hook();
        return hook.get(propertyName);
    }

    /**
     * Change the state of the object
     * @param propertyName name of the property
     * @param value the value which one wants to set
     * @return the previous value of the property
     */
    public Object change(String propertyName, Object value){
        MethodHook hook = hook();
        return hook.set(propertyName,value);
    }

    /**
     * Gets the count of invocation of the method
     * @param methodName name of the method
     * @return count of invocation
     */
    public int times(String methodName){
        MethodHook hook = hook();
        return  hook.numCalls(methodName);
    }

    /**
     * Gets the last invoked arguments
     * @param methodName name of the method
     * @return last argument, if multiple, returns an object array
     */
    public Object args(String methodName){
        MethodHook hook = hook();
        Object[] args = hook.args(methodName);
        return ( args != null && args.length == 1) ? args[0] : args ;
    }

    /**
     * Was the method ever invoked
     * @param methodName name of the method
     * @return true if called at least once, false otherwise
     */
    public boolean called(String methodName){
        try {
            return  0 != times(methodName);
        }catch (Exception e){}
        return false;
    }

    /**
     * Creates a rule
     */
    public JokeRule(){
        object = null;
        methodName = "" ;
        arguments = null ;
        returnValue = NIL;
        except = null ;
    }

    /**
     * Binds an object to rule
     * @param o the object to bind
     * @return self
     */
    public JokeRule on(Object o){
        object = o ;
        // init once and then forget, really
        hook();
        return this;
    }

    /**
     * Binds a method name to rule
     * @param method name of the method
     * @return self
     */
    public JokeRule calling(String method){
        if ( object == null ) throw new UnsupportedOperationException("Object instance is not set!");
        methodName = method ;
        return this;
    }

    /**
     * Bind arguments to a rule
     * @param args the arguments
     * @return self
     */
    public JokeRule with(Object...args){
        if ( methodName.isEmpty() ) throw new UnsupportedOperationException("Method is not set!");
        arguments = args ;
        return this;
    }

    /**
     * Binds a return value to a rule
     * @param r the return value
     */
    public void returns(Object r){
        if ( arguments == null ) throw new UnsupportedOperationException("Arguments are not set!");
        returnValue = r ;
        except = null ;
        apply();
    }

    /**
     * Binds a Throwable to a rule
     * @param e the Throwable
     */
    public void raises(Throwable e){
        if ( methodName.isEmpty() ) throw new UnsupportedOperationException("Method is not set!");
        except = e ;
        returnValue = NIL;
        apply();
    }

    private void apply(){
        MethodHook hook = hook();
        hook.applyRule(this);
    }

}
