package joker.core.tests;

import joker.core.Joker;
import org.junit.Test;
import zoomba.lang.core.types.ZString;
import java.util.Iterator;
import java.util.List;
import static org.junit.Assert.*;

public class JTest {

    private static final String DEFINITION_PATH = "definitions/Basic.zm";

    private static final String LIST_MOCK = "JListMock" ;

    private static final String ITER_MOCK = "JIteratorMock" ;

    private static final String DUMMY_MOCK = "JDummyMock" ;

    private static final String STRING_MOCK = "JStringMock" ;

    private static final String PERSISTENCE_BEHAVE_MOCK = "JDataPersistServiceMockWithBehaviour" ;

    static class Dummy{

        int someThing = 0 ;

        int getSomeThing() {
            return someThing;
        }

        void setSomeThing(int someThing) {
            this.someThing = someThing;
        }
    }

    interface DataPersistService{

        Dummy load(String name);

        boolean save( Dummy d , String name );
    }

    @Test
    public void jokeList(){
        // people asked to see if it works , it does.
        List<Integer> l = Joker.joke( LIST_MOCK , DEFINITION_PATH );
        assertNotNull(l);
        // check basis
        assertEquals(0, l.size() ) ;
        assertTrue( l.add(42) ) ; // does it return right ?
        // automatic state management
        assertEquals(1, l.size() ) ;
        try{
            l.contains(0);
            // as this is not defined, it must throw exception -->??
            assertTrue(false);
        }catch (Throwable t){
            // and yes, we must have t
            assertTrue(t.getMessage().contains("Method") );
        }
    }

    @Test
    public void jokeIterator(){
        final int max = 3;
        Iterator i = Joker.joke( ITER_MOCK, DEFINITION_PATH , max);
        int count = 0;
        while ( i.hasNext() ){ i.next(); count++; }
        assertEquals(max, count );
    }

    @Test
    public void jokeDummy(){
        Dummy d = Joker.joke( DUMMY_MOCK , DEFINITION_PATH );
        d.setSomeThing(42);
        assertEquals( 42, d.getSomeThing() );
    }

    @Test
    public void testDefaultOptions(){
        List l = Joker.joke( LIST_MOCK , DEFINITION_PATH );
        assertEquals( 0 , l.get(42)  );
        assertEquals( 42 , l.get(55)  );
        // check exceptions ?
        try{
            l.get(100); // should throw exception
            assertTrue(false);
        }catch (Throwable t){
            assertTrue(t.getMessage().contains("Joker"));
        }
    }

    @Test
    public void testStringMock(){
        ZString s = Joker.joke( STRING_MOCK, DEFINITION_PATH );
        final String charAt = "charAt";
        Joker.on(s).calling(charAt).with(0).returns('J');
        Joker.on(s).calling(charAt).with(1).raises( new IndexOutOfBoundsException("1"));
        assertEquals( 'J', s.charAt(0) );
        try{
            s.charAt(1);
            assertTrue(false);
        }catch (IndexOutOfBoundsException i){
            assertTrue(true);
        }
        assertTrue( Joker.on(s).called(charAt) );
        assertEquals( 2, Joker.on(s).times(charAt));
        assertEquals( 1, Joker.on(s).args(charAt) );
    }

    /*
     http://stackoverflow.com/questions/2684630/how-can-i-make-a-method-return-an-argument-that-was-passed-to-it
     The goal was to mock a service that persists Objects and can return them by their name.
    * */
    @Test
    public void showDifference(){
        DataPersistService s = Joker.joke( PERSISTENCE_BEHAVE_MOCK, DEFINITION_PATH );
        final String name = "x" ;
        // go normal flow
        Dummy d = new Dummy();
        d.setSomeThing(42);
        assertTrue( s.save( d , name) );
        d = s.load(name);
        assertNotNull( d );
        assertEquals( 42, d.getSomeThing() );
        assertNull( s.load("none") );

        // now test behaviour
        final String stateName = "behaviour" ;
        final String stateError = "error" ;
        // inject error --> branch behaviour
        Joker.on(s).change( stateName, stateError );
        // check if the state was indeed changed ?
        assertEquals( stateError , Joker.on(s).view(stateName));
        assertFalse(  s.save( d , "y" ) ) ;
        assertNull( s.load(name) );
        // switching behaviour should be able to save again
        Joker.on(s).change( stateName, "" );
        assertTrue( s.save(d,"y") );
        assertNotNull( s.load("y") );
    }
}