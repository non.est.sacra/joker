package joker.core;

import joker.creator.JokeRule;
import joker.creator.MethodHook;
import joker.creator.ZInterface;
import org.mockito.internal.creation.MockSettingsImpl;
import org.mockito.internal.creation.jmock.ClassImposterizer;
import static org.mockito.Mockito.* ;

/**
 */
public class Joker {

    private static final String TYPE_DEF  = "$TYPE" ;

    static <T> T jMock(Class<T> classToMock , ZInterface.ZProtoType definition, Object[] constructorArgs ){

        // let this hack run as of now ...
        Class<?>[] interfaces = ((MockSettingsImpl)withSettings()).getExtraInterfaces();
        // get along with it
        Class<?>[] ancillaryTypes = interfaces == null ? new Class<?>[0] : interfaces;
        MethodHook filter = new MethodHook( definition , constructorArgs );
        T mock = ClassImposterizer.INSTANCE.imposterise(filter, classToMock, ancillaryTypes);
        return mock ;
    }

    /**
     * Binds an object to a Joker rule
     * @param o the object
     * @return a joker rule bounded to the object
     */
    public static JokeRule on( Object o){
        JokeRule jokeRule = new JokeRule();
        return jokeRule.on(o);
    }

    /**
     * Create a Joked object
     * @param definitionName the definition name, in the ZoomBA file
     * @param definitionPath the path to the ZoomBA file
     * @param constructorArgs arguments to the constructor for the ZoomBA fake object
     * @param <T> Type of the object
     * @return the joked object from the definition and constructor args
     */
    public static <T> T joke(String definitionName, String definitionPath, Object... constructorArgs ) {
        ZInterface.ZProtoType  protoDefinition = ZInterface.protoType( definitionName, definitionPath );
        String className = String.valueOf( protoDefinition.proto.get( TYPE_DEF ) ) ;
        if ( null == className ) throw new RuntimeException( TYPE_DEF + " is not specified!");
        try{
            Class<T> clazz = (Class<T>) Class.forName( className );
            return jMock( clazz , protoDefinition , constructorArgs );
        }catch (Exception e){
            throw new RuntimeException(e);
        }
    }

}
