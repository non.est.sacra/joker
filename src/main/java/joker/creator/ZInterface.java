package joker.creator;

import zoomba.lang.core.interpreter.ZContext;
import zoomba.lang.core.interpreter.ZScript;
import zoomba.lang.core.oop.ZObject;
import zoomba.lang.core.operations.Function;

import java.io.File;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


public class ZInterface {

    private final ZScript script;

    private static final String RESOURCE_MAIN = "__JOKER__" ;

    private final ZContext.FunctionContext functionContext ;

    private static final Map<String,ZInterface>  resources =  new ConcurrentHashMap<>();

    public static final class ZProtoType{

        final ZScript script ;

        public final ZObject proto;

        ZProtoType( ZScript zs, ZObject zo){
            script = zs;
            proto = zo;
        }
    }

    private ZInterface(String resourceLocation) {
        script = new ZScript( resourceLocation, null, RESOURCE_MAIN );
        functionContext = new ZContext.FunctionContext(ZContext.EMPTY_CONTEXT, new ZContext.ArgContext());
        script.setExternalContext( functionContext );
        script.execute(); // do call for execute
    }


    private ZObject proto(String name){
        Function.MonadicContainer mc = functionContext.get(name) ;
        if ( mc.value() instanceof ZObject ) return ((ZObject) mc.value());
        throw new RuntimeException(name + " : is not a ZProto Object Definition!");
    }

    public static ZProtoType protoType(String definitionName, String definitionPath ){
        String location = definitionPath ;
        File f = new File( location );
        location = f.getAbsolutePath();
        ZInterface zi;
        if ( resources.containsKey( location ) ){
            zi = resources.get( location );
        }else{
            zi = new ZInterface( location );
            resources.put( location, zi );
        }
        return new ZProtoType( zi.script , zi.proto( definitionName ) ) ;
    }
}